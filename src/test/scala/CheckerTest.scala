import org.scalatest._

class CheckerTest extends FlatSpec {

  "A Solve" should "be correct to convert in array" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.solveArray(correctSolve) === Array(
      Array(2, 3, 8, 9, 6, 5, 7, 1, 4),
      Array(7, 5, 9, 4, 1, 3, 6, 8, 2),
      Array(4, 1, 6, 2, 7, 8, 9, 5, 3),
      Array(9, 4, 5, 1, 3, 6, 2, 7, 8),
      Array(6, 8, 7, 5, 2, 4, 1, 3, 9),
      Array(3, 2, 1, 8, 9, 7, 4, 6, 5),
      Array(1, 6, 2, 3, 5, 9, 8, 4, 7),
      Array(5, 7, 4, 6, 8, 2, 3, 9, 1),
      Array(8, 9, 3, 7, 4, 1, 5, 2, 6)))
  }

  it should "be correct input" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.correctInput(correctSolve) === true)
  }

  it should "be incorrect input(1)" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 10 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.correctInput(correctSolve) === false)
  }

  it should "be incorrect input(2)" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 -1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.correctInput(correctSolve) === false)
  }

  it should "be incorrect input(3)" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 1 3 6 8 2
        |4 100 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 -4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 -5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.correctInput(correctSolve) === false)
  }

  it should "be correct(1)" in {
    val correctSolve: String =
      """2 3 8 9 6 5 7 1 4
        |7 5 9 4 1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.check(Checker.solveArray(correctSolve)) === "корректно")
  }

  it should "be correct(2)" in {
    val correctSolve: String =
      """7 3 5 6 1 4 8 9 2
        |8 4 2 9 7 3 5 6 1
        |9 6 1 2 8 5 3 7 4
        |2 8 6 3 4 9 1 5 7
        |4 1 3 8 5 7 9 2 6
        |5 7 9 1 2 6 4 3 8
        |1 5 7 4 9 2 6 8 3
        |6 9 4 7 3 8 2 1 5
        |3 2 8 5 6 1 7 4 9""".stripMargin
    assert(Checker.check(Checker.solveArray(correctSolve)) === "корректно")
  }

  it should "be correct(3)" in {
    val correctSolve: String =
      """4 3 5 2 6 9 7 8 1
        |6 8 2 5 7 1 4 9 3
        |1 9 7 8 3 4 5 6 2
        |8 2 6 1 9 5 3 4 7
        |3 7 4 6 8 2 9 1 5
        |9 5 1 7 4 3 6 2 8
        |5 1 9 3 2 6 8 7 4
        |2 4 8 9 5 7 1 3 6
        |7 6 3 4 1 8 2 5 9""".stripMargin
    assert(Checker.check(Checker.solveArray(correctSolve)) === "корректно")
  }

  it should "be incorrect(1)" in {
    val uncorrectSolve: String =
      """2 3 8 9 6 5 7 1 5
        |7 5 9 4 1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |5 7 4 6 8 2 3 9 1
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.check(Checker.solveArray(uncorrectSolve)) === "некорректно")
  }

  it should "be incorrect(2)" in {
    val uncorrectSolve: String =
      """2 3 8 9 6 5 7 1 5
        |7 5 9 4 1 3 6 8 2
        |4 1 6 2 7 8 9 5 3
        |9 4 5 1 3 6 2 7 8
        |5 7 4 6 8 2 3 9 1
        |6 8 7 5 2 4 1 3 9
        |3 2 1 8 9 7 4 6 5
        |1 6 2 3 5 9 8 4 7
        |8 9 3 7 4 1 5 2 6""".stripMargin
    assert(Checker.check(Checker.solveArray(uncorrectSolve)) === "некорректно")
  }

  it should "be incorrect(3)" in {
    val uncorrectSolve: String =
      """2 3 8 9 6 5 7 4 1
        |7 5 9 4 1 3 6 2 8
        |4 1 6 2 7 8 9 3 4
        |9 4 5 1 3 6 2 8 7
        |6 8 7 5 2 4 1 9 3
        |3 2 1 8 9 7 4 5 6
        |1 6 2 3 5 9 8 7 4
        |5 7 4 6 8 2 3 1 9
        |8 9 3 7 4 1 5 6 2""".stripMargin
    assert(Checker.check(Checker.solveArray(uncorrectSolve)) === "некорректно")
  }

  it should "be incorrect(4)" in {
    val uncorrectSolve: String =
      """7 3 5 6 1 4 8 9 2
        |8 4 2 9 7 3 5 6 1
        |9 6 1 2 5 8 3 7 4
        |2 8 6 3 4 9 1 5 7
        |4 1 3 8 7 5 9 2 6
        |5 7 9 1 2 6 4 3 8
        |1 5 7 4 9 2 6 8 3
        |6 9 4 7 3 8 2 1 5
        |3 2 8 5 6 1 7 4 9""".stripMargin
    assert(Checker.check(Checker.solveArray(uncorrectSolve)) === "некорректно")
  }
}
