import scala.collection.mutable._

object Checker extends App {
  val solve: String =
    """2 3 8 9 6 5 7 1 4
      |7 5 9 4 1 3 6 8 2
      |4 1 6 2 7 8 9 5 3
      |9 4 5 1 3 6 2 7 8
      |6 8 7 5 2 4 1 3 9
      |3 2 1 8 9 7 4 6 5
      |1 6 2 3 5 9 8 4 7
      |5 7 4 6 8 2 3 9 1
      |8 9 3 7 4 1 5 2 6""".stripMargin

  def solveArray(solve: String): Array[Array[Int]] = solve.split("\n").map(_.split(" ").map(_.toInt))

  def check(arr: Array[Array[Int]]): String = {
    val verticalCheck: List[Set[Int]] = List.fill(9)(Set.empty)
    var flagHor: Boolean = true
    val boxCheck: List[Set[Int]] = List.fill(3)(Set.empty)
    for (line <- arr.indices) {
      if (flagHor) {
        var flagBox: Boolean = true
        val horizontalCheck: Set[Int] = Set.empty
        for (i <- arr(line).indices) {
          if (flagBox) {
            horizontalCheck += arr(line)(i)
            verticalCheck(i) += arr(line)(i)
            boxCheck(i / 3) += arr(line)(i)
            if (((line + 1) % 3 == 0) && ((i + 1) % 3 == 0)) {
              flagBox = nineElemCheck(boxCheck(i / 3))
            }
          }
        }
        flagHor = nineElemCheck(horizontalCheck) && flagBox
      }
    }
    flagHor && verticalCheck.forall(set => nineElemCheck(set)) match {
      case true => "корректно"
      case false => "некорректно"
    }
  }

  def nineElemCheck(el: Set[Int]): Boolean = {
    (el.sum == 45) || (el.size == 9)
  }

  def correctInput(solve: String): Boolean = {
    solveArray(solve).map(ar => ar.forall(x => x > 0 && x < 10)).forall(_ == true)
  }

  def checker(solve: String): Unit = {
    correctInput(solve) match {
      case true => println(check(solveArray(solve)))
      case false => println("некорректно")
    }
  }

  checker(solve)
}
