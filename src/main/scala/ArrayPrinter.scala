object ArrayPrinter {
  def printArray[T](array: Array[T]): Unit = {
    for (i <- array) {
      i match {
        case ar: Array[_] => printArray(ar)
        case _ => print(i + " ")
      }
    }
    println()
  }
}
